{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

{- | Bindings to the Botan3 implementation of ZFEC.
 | Work-in-progress.
-}
module ZFEC (encode, encodePadded, decode, decodePadded) where

import Control.Monad (join, zipWithM)
import qualified Data.ByteString as B
import Data.Word (Word8)
import Foreign.Marshal.Alloc (allocaBytes)
import Foreign.Marshal.Array (allocaArray, peekArray, withArray)
import Foreign.Ptr (Ptr, castPtr)
import Foreign.Storable (pokeElemOff, sizeOf)

foreign import ccall unsafe "botan_zfec_encode" zfecEncode :: Word -> Word -> Ptr Word8 -> Word -> Ptr (Ptr Word8) -> Ptr Word -> IO Int

-- zfecEncode k n input size outputs sizes

foreign import ccall unsafe "botan_zfec_decode" zfecDecode :: Word -> Word -> Ptr Word -> Ptr (Ptr Word8) -> Word -> Ptr (Ptr Word8) -> Ptr Word -> IO Int

-- zfecDecode k n indexes inputs size outputs sizes

-- | Determine the number of bytes in a block
computeBlockSize :: Int -> Int -> Maybe Int
computeBlockSize totalSize k
    | totalSize `mod` k == 0 = Just $ totalSize `div` k
    | otherwise = Nothing

withArrayOfWord8s :: Int -> Int -> (Ptr (Ptr Word8) -> IO a) -> IO a
withArrayOfWord8s n blockSize f =
    allocaArray n $ \outerBuf ->
        withArrayOfWord8s' n blockSize outerBuf f
  where
    withArrayOfWord8s' :: Int -> Int -> Ptr (Ptr Word8) -> (Ptr (Ptr Word8) -> IO a) -> IO a
    withArrayOfWord8s' 0 _ outerBuf f = f outerBuf
    withArrayOfWord8s' n blockSize outerBuf f = do
        allocaArray blockSize $ \innerBuf -> do
            pokeElemOff outerBuf (n - 1) innerBuf
        withArrayOfWord8s' (n - 1) blockSize outerBuf f

encode :: Int -> Int -> B.ByteString -> IO (Either Int [B.ByteString])
encode k n input =
    case computeBlockSize (B.length input) k of
        Nothing -> pure $ Left (-42)
        Just blockSize -> do
            withArrayOfWord8s n blockSize $ \outputBlocks -> do
                allocaBytes (n * sizeOf (undefined :: Word)) $ \outputSizes -> do
                    B.useAsCStringLen input $ \(inputPtr, inputLen) -> do
                        result <- zfecEncode (toWord k) (toWord n) (castPtr inputPtr) (toWord inputLen) outputBlocks outputSizes
                        case result of
                            0 -> do
                                outputs <- map castPtr <$> peekArray n outputBlocks
                                sizeWords <- peekArray n outputSizes
                                -- botan_zfec_encode has given us the memory it
                                -- malloc()'d for all of these uint8_t*.  Botan will
                                -- never touch the memory again - even to free it.
                                -- Therefore, we must arrange for it to be freed and
                                -- we can also be confident the underlying bytes won't
                                -- change.  Given this, we are confident that in this
                                -- case unsafePackCStringLen is actually safe.
                                encoded <- mapM B.packCStringLen $ zip outputs (map fromWord sizeWords)
                                return $ Right encoded
                            e -> return $ Left e

{- | Pad the input string so its length is a multiple of K as required by
 encode.  The length of the padding is encoded in the padding byte itself.
 The length of the padding always fits into a one byte integer.
-}
encodePadded :: Int -> Int -> B.ByteString -> IO (Either Int [B.ByteString])
encodePadded k n input =
    encode k n (input <> padding paddingLength)
  where
    paddingLength :: Word8 =
        case B.length input `mod` k of
            0 -> toWord8 k
            excess ->
                if k - excess <= 256
                    then toWord8 (k - excess)
                    else error $ "encodePadded: k - n out of bounds: " <> show (k - excess)

padding :: Word8 -> B.ByteString
padding n = B.replicate (fromWord8 n - 1) 0 `B.append` B.singleton n

data DecodeError
    = WrongNumberOfInputBlocks
        { wrongNumberActual :: Int
        , wrongNumberExpected :: Int
        }
    | InvalidEncodingParameters
        { invalidEncodingK :: Int
        , invalidEncodingN :: Int
        }
    | MismatchedInputBlockLengths
    | FFIError
        { ffiErrorValue :: Int
        }
    deriving (Show, Ord, Eq)

decodePadded :: Int -> Int -> [(Int, B.ByteString)] -> IO (Either DecodeError B.ByteString)
decodePadded k n taggedBlocks =
    (unPad . B.concat <$>) <$> decode k n taggedBlocks
  where
    unPad :: B.ByteString -> B.ByteString
    unPad padded =
        B.take (B.length padded - paddingLength) padded
      where
        paddingLength = fromWord8 $ B.last padded

decode :: Int -> Int -> [(Int, B.ByteString)] -> IO (Either DecodeError [B.ByteString])
decode k n taggedBlocks
    | k == 0 = pure . Left $ InvalidEncodingParameters k n
    | k /= length taggedBlocks = pure . Left $ WrongNumberOfInputBlocks (length taggedBlocks) k
    | not (sameLengths taggedBlocks) = pure . Left $ MismatchedInputBlockLengths
    | otherwise =
        let (tags, blocks) = unzip taggedBlocks
            blockSize = toWord (B.length (head blocks))
         in withDecodeBuffers k tags blocks $ \decodedBlocks decodedSizes indexesArray cStringArray -> do
                result <- zfecDecode (toWord k) (toWord n) indexesArray cStringArray blockSize decodedBlocks decodedSizes

                if result == 0
                    then do
                        -- The call succeeded so our decoded data is available in
                        -- the elements of decodedBlocks.  Read it out.
                        byteStrings <- join $ zipWithM asByteString <$> peekArray k decodedBlocks <*> (pure . repeat $ blockSize)
                        pure . Right $ byteStrings
                    else pure $ Left $ FFIError result

toWord :: Int -> Word
toWord = fromIntegral

toWord8 :: Int -> Word8
toWord8 = fromIntegral

fromWord :: Word -> Int
fromWord = fromIntegral

fromWord8 :: Word8 -> Int
fromWord8 = fromIntegral

asByteString :: Ptr Word8 -> Word -> IO B.ByteString
asByteString ptr len = B.packCStringLen (castPtr ptr, fromWord len)

useAsCStrings :: [B.ByteString] -> (Ptr (Ptr Word8) -> IO a) -> IO a
useAsCStrings byteStrings f =
    -- Allocate enough memory to pointers to all of the C strings we're making.
    allocaBytes (length byteStrings * sizeOf (undefined :: Ptr Word8)) $ \byteArray ->
        -- Create a C string for each ByteString we got and put a pointer to it into
        -- the array then call the function with the array.
        useAsCStringsInArray byteStrings 0 byteArray f

useAsCStringsInArray :: [B.ByteString] -> Int -> Ptr (Ptr Word8) -> (Ptr (Ptr Word8) -> IO a) -> IO a
useAsCStringsInArray [] _ byteArray op = op byteArray
useAsCStringsInArray (bs : bss) pos byteArray op = do
    B.useAsCString bs $ \cString -> do
        -- Put this CString into the array.
        pokeElemOff byteArray pos (castPtr cString)
        -- Put the rest of the CStrings into the rest of the array
        useAsCStringsInArray bss (pos + 1) byteArray op

sameLengths :: [(a, B.ByteString)] -> Bool
sameLengths [] = True
sameLengths ((_, b) : bs) = all ((B.length b ==) . B.length . snd) bs

withDecodeBuffers :: Int -> [Int] -> [B.ByteString] -> (Ptr (Ptr Word8) -> Ptr Word -> Ptr Word -> Ptr (Ptr Word8) -> IO a) -> IO a
withDecodeBuffers k tags blocks f =
    -- Allocate enough memory to store pointers to all of the output data
    withArrayOfWord8s k (B.length $ head blocks) $ \decodedBlocks ->
        -- Allocate enough memory to store pointers to the sizes of all the output data
        allocaBytes (k * sizeOf (undefined :: Word)) $ \decodedSizes ->
            -- Convert all of the tags to a Ptr Word that we can pass in to the decoder.
            withArray tags $ \indexesArray ->
                -- Convert all of the input blocks to c strings that we can pass in to the
                -- decoder
                useAsCStrings blocks $ \cStringArray ->
                    -- Run the operation with all that memory.
                    f decodedBlocks decodedSizes (castPtr indexesArray) cStringArray
