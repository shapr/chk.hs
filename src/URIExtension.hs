module URIExtension (
    URIExtension (..),
    uriExtensionToBytes,
    showBytes,
) where

import Data.Text.Format (
    format,
 )

import Control.Monad (
    join,
 )
import Data.ByteString.Base32 (
    encodeBase32Unpadded,
 )

import qualified Data.ByteString as B
import qualified Data.Text as T
import Data.Text.Encoding (
    decodeUtf8,
    encodeUtf8,
 )
import qualified Data.Text.Lazy as TL

import Data.List (
    sort,
 )

import Types

import Netstring (
    netstring,
 )

data URIExtension = URIExtension
    { uriExtCodecName :: B.ByteString
    , uriExtCodecParams :: Parameters
    , uriExtTailCodecParams :: Parameters
    , uriExtSize :: Size
    , uriExtSegmentSize :: Size
    , uriExtNumSegments :: SegmentNum
    , uriExtNeededShares :: Required
    , uriExtTotalShares :: Total
    , uriExtCrypttextHash :: CrypttextHash
    , uriExtCrypttextRootHash :: CrypttextHash -- XXX MerkelHash?
    , uriExtShareRootHash :: CrypttextHash -- XXX MerkleHash?
    }

instance Show URIExtension where
    show (URIExtension name params tailParams size segSize numSegs needed total hash1 hash2 hash3) =
        TL.unpack $ format "URIExtension { codec = {}; codec-params = {}; tail-codec-params = {}; size = {}; segment-size = {}; num-segments = {}; needed-shares = {}; total-shares = {}; crypttext-hash = {}; crypttext-root-hash = {}; share-root-hash = {} }" (utf8 name, show params, show tailParams, size, segSize, numSegs, needed, total, b32 hash1, b32 hash2, b32 hash3)
      where
        b32 = encodeBase32Unpadded
        utf8 = decodeUtf8

uriExtensionToBytes :: URIExtension -> B.ByteString
uriExtensionToBytes =
    toWeirdString
        -- all of the below values are authenticated by the capability you get when you store data in Tahoe
        [ ("codec_name", uriExtCodecName)
        , ("codec_params", paramsToBytes . uriExtCodecParams)
        , ("tail_codec_params", paramsToBytes . uriExtTailCodecParams)
        , ("size", showBytes . uriExtSize)
        , ("segment_size", showBytes . uriExtSegmentSize)
        , ("num_segments", showBytes . uriExtNumSegments)
        , ("needed_shares", showBytes . uriExtNeededShares)
        , ("total_shares", showBytes . uriExtTotalShares)
        , ("crypttext_hash", uriExtCrypttextHash) -- hash of the *entire* cipher text
        , ("crypttext_root_hash", uriExtCrypttextRootHash) -- root hash of the *cipher text* merkle tree
        , ("share_root_hash", uriExtShareRootHash) -- root hash of the *share* merkle tree
        ]

toWeirdString :: [(B.ByteString, URIExtension -> B.ByteString)] -> URIExtension -> B.ByteString
toWeirdString fields ext =
    B.concat . join . sort $ map (encodedField ext) fields
  where
    encodedField ext (name, extract) =
        [name, ":", netstring (extract ext)]

showBytes :: (Show s) => s -> B.ByteString
showBytes = encodeUtf8 . T.pack . show

paramsToBytes :: Parameters -> B.ByteString
paramsToBytes (Parameters segmentSize total _ required) =
    B.concat [showBytes segmentSize, "-", showBytes required, "-", showBytes total]
