module Types where

import Data.Word (
    Word8,
 )

import qualified Data.ByteString as B
import qualified Data.Text as T

import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

import Crypto.Cipher.AES128 (
    AESKey128,
 )

-- How much data is there
type Size = Integer

-- Byte-based position into a share
type Offset = Integer

-- Segment-based position into a share
type SegmentNum = Int

-- With respect to FEC encoding, the number of a share.
type ShareNum = Word8

type ShareMap = Map.Map ShareNum (Set.Set StorageServer)

-- The SHA256d hash of a FEC-encoded block
type BlockHash = B.ByteString

-- The SHA256d hash of some ciphertext
type CrypttextHash = B.ByteString

type BlockHashMap = Map.Map ShareNum [BlockHash]

-- Erasure encoding / placement parameters
type Total = Int
type Happy = ShareNum -- This is not like the others.
type Required = Int
type SegmentSize = Size
data Parameters = Parameters
    { paramSegmentSize :: SegmentSize
    , paramTotalShares :: Total
    , paramHappyShares :: Happy
    , paramRequiredShares :: Required
    }
    deriving (Show, Ord, Eq)

-- 16 bytes
type StorageIndex = B.ByteString

-- Where can a server be found
type URL = T.Text

-- The unique identifier for a particular storage server
type StorageServerID = T.Text

-- A value that partially grants the ability to operate on some stored data
-- somehow.
data Capability = Capability
    { capabilityText :: T.Text
    }
    deriving (Show, Eq)

-- A server that can have some data uploaded to it.
data StorageServer = StorageServer
    { storageServerID :: StorageServerID
    , storageServerWrite :: StorageIndex -> ShareNum -> Offset -> B.ByteString -> IO ()
    , storageServerRead :: StorageIndex -> ShareNum -> IO B.ByteString
    , storageServerGetBuckets :: StorageIndex -> IO (Set.Set ShareNum)
    }

instance Eq StorageServer where
    a == b = storageServerID a == storageServerID b

instance Ord StorageServer where
    a <= b = storageServerID a <= storageServerID b

instance Show StorageServer where
    show ss = show $ storageServerID ss

-- Some data that can be uploaded.
data Uploadable = Uploadable
    { uploadableKey :: AESKey128
    , uploadableSize :: Size
    , uploadableParameters :: Parameters
    , uploadableReadCleartext :: Integer -> IO B.ByteString
    }

-- The outcome of an attempt to upload an immutable.
data UploadResult = UploadResult
    { uploadResultReadCap :: Capability
    , uploadResultExistingShares :: Integer
    , uploadResultShareMap :: ShareMap
    }
    deriving (Show)
