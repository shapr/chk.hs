{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Vectors where

import GHC.Generics (
    Generic,
 )

import qualified Data.Text as T
import qualified Data.Text.Encoding as T

import Control.Exception (
    Exception,
 )

import Data.Map (
    Map,
 )

import qualified Data.ByteString as B

import Data.Aeson (
    FromJSON (..),
    ToJSON (..),
    Value (..),
    toJSONList,
    (.:),
 )

import Data.Scientific (
    Scientific,
    toBoundedInteger,
 )

import Data.Aeson.Types (
    Parser,
    parseFail,
    prependFailure,
    typeMismatch,
    withArray,
    withObject,
    withScientific,
    withText,
 )

import Data.Vector (
    (!),
 )
import qualified Data.Vector (
    length,
 )

import Data.ByteString.Base64 (
    decodeBase64,
    encodeBase64,
 )

import Types (
    Parameters (..),
    StorageServer (..),
 )

newtype JSONByteString = JSONByteString B.ByteString deriving newtype (Ord, Eq)

instance Show JSONByteString where
    show (JSONByteString bs) = T.unpack . encodeBase64 $ bs

instance FromJSON JSONByteString where
    parseJSON (String t) =
        case decodeBase64 . T.encodeUtf8 $ t of
            Left err -> parseFail "parsing base64-encoded byte string failed"
            Right stuff -> pure $ JSONByteString stuff

instance ToJSON JSONByteString where
    toJSON (JSONByteString bs) = String $ encodeBase64 bs

newtype ZFECParameters = ZFECParameters Parameters deriving newtype (Show, Ord, Eq)

data SSKFormat
    = SDMF
        { sskPrivateKey :: T.Text
        }
    | MDMF
        { sskPrivateKey :: T.Text
        }
    deriving (Show, Ord, Eq)

data Format = CHK | SSK SSKFormat deriving (Show, Ord, Eq)

instance FromJSON Format where
    parseJSON = withObject "format" $ \o -> do
        kind <- o .: "kind"
        case kind of
            "chk" -> pure CHK
            "ssk" -> SSK <$> o .: "params"
            invalid -> parseFail $ "Unsupported format: " <> T.unpack invalid

instance FromJSON SSKFormat where
    parseJSON = withObject "ssk-format" $ \o -> do
        format <- o .: "format"
        key <- o .: "key"
        sskFormat <- case format of
            "sdmf" -> pure SDMF
            "mdmf" -> pure MDMF
            invalid -> parseFail $ "Unsupported SSK format: " <> T.unpack invalid
        pure $ sskFormat key

data Sample = Sample
    { sampleTemplate :: JSONByteString
    , sampleLength :: Int
    }
    deriving (Show, Ord, Eq)

instance FromJSON Sample where
    parseJSON = withObject "sample" $ \o ->
        Sample <$> o .: "seed" <*> o .: "length"

data VectorSpec = VectorSpec
    { version :: T.Text
    , vector :: [TestCase]
    }
    deriving (Generic, Show, Ord, Eq, FromJSON)

data TestCase = TestCase
    { convergence :: JSONByteString
    , format :: Format
    , sample :: Sample
    , zfec :: Parameters
    , expected :: T.Text
    }
    deriving (Generic, Show, Ord, Eq, FromJSON)

instance FromJSON Parameters where
    parseJSON = withObject "parameters" $ \o ->
        Parameters <$> o .: "segmentSize" <*> o .: "total" <*> pure 1 <*> o .: "required"
