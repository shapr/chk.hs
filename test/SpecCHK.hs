{-# LANGUAGE OverloadedStrings #-}

module SpecCHK (
    tests,
) where

import Control.Arrow (
    (&&&),
 )

import Data.ByteString.Base64 (
    decodeBase64,
 )

import Data.Either (
    fromRight,
 )

import Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import Data.Coerce (coerce)

import Data.Text (
    Text,
    unpack,
 )

import Data.ByteString.Base64 (
    encodeBase64,
 )

import Test.Tasty (
    TestTree,
    testGroup,
 )

import Test.Tasty.HUnit (
    Assertion,
    assertEqual,
    testCase,
 )
import Test.Tasty.Hedgehog

import Types (
    Capability (..),
    Parameters (..),
    UploadResult (..),
    Uploadable (Uploadable),
 )

import Upload (
    adjustSegmentSize,
    getConvergentKey,
    memoryUploadableWithConvergence,
    store,
 )

import CHK (
    chkEncrypt,
 )

import Crypto.Cipher.AES128 (
    AESKey128,
 )

import Crypto.Classes (
    encode,
 )

import Vectors (
    Format (..),
    JSONByteString (..),
    Sample (..),
    TestCase (..),
    VectorSpec (..),
 )

import NullServer (
    directoryServer,
    nullStorageServer,
 )

tests :: VectorSpec -> TestTree
tests vectorSpec =
    testGroup
        "CHK"
        [ testCap vectorSpec
        , testEncrypt
        , testProperty "expand returns the correct number of bytes" prop_expand_length
        , testProperty "expand returns bytes containing the template repeated" prop_expand_template
        ]

testEncrypt :: TestTree
testEncrypt =
    testGroup
        "chkEncrypt"
        [ testCase "ciphertext" $ do
            assertEqual
                "expected convergence key"
                "oBcuR/wKdCgCV2GKKXqiNg=="
                (encodeBase64 $ encode convergenceKey)
            ciphertext <- encrypt
            let b64ciphertext = encodeBase64 ciphertext
            assertEqual "known result" knownCorrect b64ciphertext
        ]
  where
    -- For all the magic values see
    -- allmydata.test.test_upload.FileHandleTests.test_get_encryption_key_convergent
    knownCorrect :: Text
    knownCorrect = "Jd2LHCRXozwrEJc="

    plaintext :: B.ByteString
    plaintext = "hello world"

    convergenceKey :: AESKey128
    convergenceKey = getConvergentKey convergenceSecret params (BL.fromStrict plaintext)

    convergenceSecret = B.replicate 16 0x42
    params =
        adjustSegmentSize
            Parameters
                { paramSegmentSize = 128 * 1024
                , paramTotalShares = 10
                , paramHappyShares = 5
                , paramRequiredShares = 3
                }
            (fromIntegral $ B.length plaintext)

    encrypt :: IO B.ByteString
    encrypt = do
        readFunction <- chkEncrypt convergenceKey (\_n -> return plaintext)
        readFunction 1024

-- testCap :: VectorSpec -> TestTree
-- testCap _ =
--   testGroup "chkCap"
--   [ testCase "hard-coded" (
--       testOneCase
--         TestCase
--         { convergence = JSONByteString (fromRight undefined $ decodeBase64 "YWFhYWFhYWFhYWFhYWFhYQ==")
--         , expected = "URI:CHK:ycvogzi6wllnq2bkx3t6zdtwju:um42l4yen7jiwdfgirvedtty3tt3xuhjiyxzqoourvughtxjar3q:1:3:1024"
--         , format = CHK
--         , sample = Sample
--                    { sampleLength = 1024
--                    , sampleTemplate = JSONByteString (fromRight undefined $ decodeBase64 "YQ==")
--                    }
--         , zfec = Parameters
--                  { paramSegmentSize = 131072
--                  , paramRequiredShares = 1
--                  , paramTotalShares = 3
--                  , paramHappyShares = 1
--                  }
--         })
--   ]
testCap :: VectorSpec -> TestTree
testCap = testGroup "chkCap" . map (uncurry ($) . (testCase . unpack . expected &&& testOneCase)) . filter pickCase . vector
  where
    pickCase TestCase{format} = format == CHK

testOneCase :: TestCase -> Assertion
testOneCase
    TestCase
        { convergence
        , format = CHK
        , sample
        , zfec
        , expected
        } =
        do
            uploadable <- memoryUploadableWithConvergence (coerce convergence) (fromIntegral $ sampleLength sample) (BL.fromStrict $ expand sample) zfec
            upresult <- store [directoryServer "storage"] uploadable
            assertEqual "yes" (Capability expected) (uploadResultReadCap upresult)

expand :: Sample -> B.ByteString
expand (Sample sampleTemplate sampleLength) =
    B.take sampleLength . B.concat $ take sampleLength (replicate n bs)
  where
    n = (sampleLength `div` (B.length $ bs)) + 1
    bs = coerce sampleTemplate -- yuck

prop_expand_length :: Property
prop_expand_length =
    property $ do
        sample <- forAll $ Sample <$> (JSONByteString <$> Gen.bytes (Range.linear 1 16)) <*> Gen.int (Range.linear 1 1000)
        diff (sampleLength sample) (==) (B.length $ expand sample)

prop_expand_template :: Property
prop_expand_template =
    property $ do
        template <- forAll $ Gen.bytes (Range.linear 1 16)
        sample <- forAll $ Sample (JSONByteString template) <$> Gen.int (Range.linear 1 1000)
        assert $ checkTemplate template (expand sample)
  where
    checkTemplate :: B.ByteString -> B.ByteString -> Bool
    checkTemplate _ "" = True
    checkTemplate template expanded =
        all (uncurry (==)) (B.zip template expanded)
            && checkTemplate template (B.drop (B.length template) expanded)
