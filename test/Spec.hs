module Main where

import Data.Yaml (
    decodeEither',
 )
import Test.Tasty (TestTree, defaultMain, testGroup)

import qualified Data.ByteString as B
import qualified SpecCHK
import qualified SpecCrypto
import qualified SpecMerkle
import qualified SpecUpload
import qualified SpecZFEC

tests :: [TestTree]
tests =
    [ SpecUpload.tests
    , SpecCrypto.tests
    , SpecMerkle.tests
    , SpecZFEC.tests
    ]

main :: IO ()
main = do
    -- TODO Handle this reading better
    test_vectors <- B.readFile "test_vectors.yaml"
    case decodeEither' test_vectors of
        Left err -> error $ "Failed to decode test vectors: " ++ show err
        Right test_vectors' ->
            defaultMain $ testGroup "all of it" $ SpecCHK.tests test_vectors' : tests
