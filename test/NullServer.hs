module NullServer where

import Types (
    StorageServer (..),
 )

import Data.ByteString.Base64 (
    encodeBase64,
 )

import System.Directory (
    createDirectoryIfMissing,
 )
import System.FilePath (
    takeDirectory,
 )

import Data.ByteString as BS
import qualified Data.Text as T
import System.IO (
    IOMode (..),
    SeekMode (..),
    hSeek,
    withFile,
 )

import Control.Exception (
    Exception,
    throwIO,
 )

nullStorageServer :: StorageServer
nullStorageServer =
    StorageServer
        { storageServerID = "null-server"
        , storageServerWrite = \_index _sharenum _offset _data -> return ()
        , storageServerRead = \_index _sharenum -> throwIO IThrewYourDataAway
        , storageServerGetBuckets = \_index -> return mempty
        }

data ReadError = IThrewYourDataAway deriving (Show)
instance Exception ReadError

directoryServer :: FilePath -> StorageServer
directoryServer p =
    StorageServer
        { storageServerID = T.pack p
        , storageServerWrite = \index sharenum offset sharedata -> do
            let path = sharePath p (T.unpack $ encodeBase64 index) sharenum
            createDirectoryIfMissing True (takeDirectory path)
            withFile path ReadWriteMode $ \f -> do
                hSeek f AbsoluteSeek offset
                BS.hPut f sharedata
        , storageServerRead = \index sharenum ->
            withFile (sharePath p (T.unpack $ encodeBase64 index) sharenum) ReadMode BS.hGetContents
        , storageServerGetBuckets = \_index -> return mempty
        }
  where
    sharePath p index sharenum = p <> "/" <> index <> "/" <> show sharenum
