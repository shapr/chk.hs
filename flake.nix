{
  description = "tahoe-lafs-immutable-uploader";

  inputs = {
    # Nix Inputs
    nixpkgs.url = github:nixos/nixpkgs/?ref=nixos-22.11;
    flake-utils.url = github:numtide/flake-utils;
    hs-flake-utils.url = "git+https://whetstone.private.storage/jcalderone/hs-flake-utils.git?ref=main";
    hs-flake-utils.inputs.nixpkgs.follows = "nixpkgs";

    botan-src = {
      flake = false;
      # We're using the unreleased (unmerged, even) Botan ZFEC C API.
      url = "github:PrivateStorageio/botan?ref=3190.ffi-zfec";
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    hs-flake-utils,
    botan-src,
  }: let
    ulib = flake-utils.lib;
  in
    ulib.eachSystem ["x86_64-linux" "aarch64-darwin"] (system: let
      hslib = hs-flake-utils.lib {
        pkgs = nixpkgs.legacyPackages.${system};
        src = ./.;
        compilerVersion = "ghc8107";
        packageName = "tahoe-lafs-immutable-uploader";
      };
    in {
      checks = hslib.checks {};
      devShells = hslib.devShells {
        extraBuildInputs = pkgs:
          with pkgs; [
            pkg-config
            ((botan2.overrideAttrs (old: {
                # Use the source we got as an input.
                src = botan-src;
              }))
              .override (old: {
                # Also, turn on debugging.
                extraConfigureFlags = "--debug-mode";
              }))
          ];
      };
      packages = hslib.packages {};
    });
}
